<?php

use Ox3a\MysqlLogParser\EventModel;
use Ox3a\MysqlLogParser\Parser;

require __DIR__ . '/../vendor/autoload.php';

$file = $argv[1] ?? null;

$parser     = Parser::get('general');
$schemaList = [];
$fileSize   = filesize($file);
$parser->on(
    'parse',
    function (EventModel $event) use (&$schemaList, $fileSize) {
        if ($event->type == 'Connect') {
            $schema = $event->extra['schema'];
            if (!in_array($schema, $schemaList)) {
                $schemaList[] = $event->extra['schema'];
            }
        } elseif ($event->type == 'Query') {
            if (strtolower(substr($event->data, 0, 3)) == 'set') {
                echo $event->data . PHP_EOL;
            }
        }

        printf("% 6.2f%%%s", 100 * $event->position / $fileSize, PHP_EOL);
    }
);

$parser->parse($file);

print_r($schemaList);
