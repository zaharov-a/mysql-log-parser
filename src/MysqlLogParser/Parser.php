<?php


namespace Ox3a\MysqlLogParser;


use Ox3a\MysqlLogParser\AbstractRow\RowParserInterface;
use Ox3a\MysqlLogParser\ErrorLog\ErrorRowParser;
use Ox3a\MysqlLogParser\GeneralLog\GeneralRowParser;
use RuntimeException;

class Parser
{
    /**
     * @var RowParserInterface
     */
    private $rowParser;

    /**
     * @var int
     */
    private $position;


    public static function get($type): Parser
    {
        return new self(self::getRowParser($type));
    }


    public static function getRowParser($type)
    {
        switch ($type) {
            case 'error':
                return new ErrorRowParser();

            case 'general':
                return new GeneralRowParser();
            default:
                throw new RuntimeException('');
        }
    }


    /**
     * Parser constructor.
     * @param RowParserInterface $rowParser
     */
    public function __construct(RowParserInterface $rowParser)
    {
        $this->rowParser = $rowParser;

        $rowParser->init();

        $this->rowParser->on(
            'parse',
            function (EventModel $event) {
                $event->position = $this->position;
            }
        );
    }


    public function on($eventName, $callback)
    {
        $this->rowParser->on($eventName, $callback);
    }


    /**
     * @param string $file
     */
    public function parse(string $file): void
    {
        $this->position = 0;

        if (!is_file($file)) {
            throw new RuntimeException('File not found');
        }

        if (!($fh = fopen($file, 'r'))) {
            throw new RuntimeException('Cannot open the file');
        }

        $lastRow = '';
        while (($rowData = fgets($fh))) {
            if ($this->rowParser->isNewRow($rowData)) {
                $this->position += strlen($lastRow);
                $this->rowParser->parse($lastRow);

                $lastRow = '';
            }
            $lastRow .= $rowData;
        }

        fclose($fh);
    }
}
