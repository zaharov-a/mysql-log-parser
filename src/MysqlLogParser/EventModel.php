<?php
/**
 * {Template_Description_Abstract}
 *
 * @author    Zakharov.A
 * @date      11.05.2021
 * @copyright {Template_Description_Copyrights}
 */

namespace Ox3a\MysqlLogParser;


use DateTimeInterface;

class EventModel
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var DateTimeInterface
     */
    public $time;

    /**
     * @var string
     */
    public $threadId;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $data;

    /**
     * @var array
     */
    public $extra = [];

    /**
     * @var int|null
     */
    public $position;


    /**
     * EventModel constructor.
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }


}
