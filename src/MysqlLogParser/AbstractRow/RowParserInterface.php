<?php

namespace Ox3a\MysqlLogParser\AbstractRow;

use Ox3a\MysqlLogParser\EventModel;

interface RowParserInterface
{

    public function init(): void;


    public function parse(string $row): ?EventModel;


    public function isNewRow(string $row): bool;


    public function on(string $eventName, callable $callback): RowParserInterface;
}
