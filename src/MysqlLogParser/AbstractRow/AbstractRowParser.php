<?php


namespace Ox3a\MysqlLogParser\AbstractRow;


use DateTimeImmutable;
use Exception;
use Ox3a\MysqlLogParser\EventModel;
use RuntimeException;

abstract class AbstractRowParser implements RowParserInterface
{
    private $startRowRegExp = '/^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{6}Z/';

    private $listeners = [
        'parse' => [],
    ];


    public function isNewRow(string $row): bool
    {
        return preg_match($this->startRowRegExp, $row);
    }


    /**
     * @param string $row
     * @return EventModel
     * @throws Exception
     */
    public function parse(string $row): ?EventModel
    {
        if (!$this->isNewRow($row)) {
            return null;
        }

        $event = new EventModel('parse');

        [$time, $event->threadId, $event->type, $data] = preg_split("/(\s|\t)+/", $row, 4) + array_fill(0, 3, null);

        $event->time = new DateTimeImmutable($time);
        $event->data = trim($data);

        $this->emit($event);

        return $event;
    }


    /**
     * @param string   $eventName
     * @param callable $callback
     * @return $this
     */
    public function on(string $eventName, callable $callback): RowParserInterface
    {
        if (!array_key_exists($eventName, $this->listeners)) {
            throw new RuntimeException(sprintf('Неизвестный тип события: %s', $eventName));
        }

        $this->listeners[$eventName][] = $callback;
        return $this;
    }


    public function emit(EventModel $event)
    {
        foreach ($this->listeners[$event->name] as $listener) {
            call_user_func($listener, $event);
        }
    }
}
