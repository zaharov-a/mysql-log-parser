<?php


namespace Ox3a\MysqlLogParser\GeneralLog;


use Ox3a\MysqlLogParser\AbstractRow\AbstractRowParser;
use Ox3a\MysqlLogParser\EventModel;

class GeneralRowParser extends AbstractRowParser
{
    public function init(): void
    {
        $this->on(
            'parse',
            function (EventModel $eventModel) {
                if ($eventModel->type == 'Connect') {
                    $eventModel->extra = $this->parseConnectData($eventModel->data);
                }
            }
        );
    }


    public function parseConnectData($data): array
    {
        if (strpos($data, 'Access') === 0) {
            [$accessStatus, $userData] = explode(' for user ', $data);
            [$access, $status] = explode(' ', $accessStatus);

            [$userHost, $passwordData] = explode(' ', $userData, 2);
            [$user, $host] = explode('@', $userHost);


            return [
                'access'   => $status,
                'user'     => trim($user, "'"),
                'host'     => trim($host, "'"),
                'password' => preg_replace('/^.+:\s([^)]+)\)$/', '$1', $passwordData),
            ];
        }

        [$userHost, $schemaType] = explode(' on ', $data);

        [$schema, $type] = explode(' using ', $schemaType);
        [$user, $host] = explode('@', $userHost);

        return [
            'access' => 'allowed',
            'user'   => $user,
            'host'   => $host,
            'schema' => $schema,
            'type'   => $type,
        ];
    }
}
